from flask import Flask, render_template, url_for, abort, redirect, flash
from address import Address
from note import Note
from forms import AddressForm

app = Flask(__name__)
app.config['SECRET_KEY'] = '1234'

addresses = [Address("Ameqran Corine", "379 Garfield Ave.", "Montreal", "Quebec"), Address("Sixto Karine", "16 SW. Thomas St.", "Montreal", "Quebec"), Address("Louiza Apurva", "353 Piper Street", "Ottawa", "Ontario")]
notes = [Note(1, "This is note 1"), Note(2, "This is note 2"), Note(3, "This is note 3")]

@app.errorhandler(404)
def page_not_found(e):
    return render_template('custom404.html'), 404

@app.route("/")
def home():
    return render_template('home.html')

@app.route("/addressbook/<string:name>")
def address_book_search(name):
    if name == "":
        return redirect(url_for('address_book'))
    for a in addresses:
        if a.name == name:
            return render_template('specific_address.html', address=a)
    
    flash("Address can not be found", 'error')
    return render_template('address.html')
            
@app.route("/addressbook", methods=['GET', 'POST'])
def address_book():
    address_form = AddressForm()
    if address_form.validate_on_submit():
        new_address = Address(address_form.name.data, address_form.street.data, address_form.city.data, address_form.province.data)

        for a in addresses:
            if a.str() == new_address.str():
                flash('Address already exist', 'error')
                return redirect(url_for('address_book'))

        addresses.append(new_address)
        flash('Address successfully added', 'success')
        return redirect(url_for('address_book'))

    return render_template('address.html', addresses=addresses, form=address_form)

@app.route("/notes/<int:id>")
def note_search(id):
    if id == "":
        return redirect(url_for('note_book'))
    for n in notes:
        if n.id == id:
            return n.str()
    
    return redirect(url_for('note_book'))            
            
@app.route("/notes")
def note_book():
    list = "<ul>"
    for n in notes:
        list += f"<li><a href='{url_for('note_search', id=n.id)}'>Note {n.id}</a></li>"
        
    list += "</ul>"
    
    return list