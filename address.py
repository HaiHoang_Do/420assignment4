class Address:
    def __init__(self, name, street, city, province):
        if name == "" or street == "" or city == "" or province == "":
            raise Exception("Inputs cannot be empty")
        else:
            self.name = name
            self.street = street
            self.city = city 
            self.province = province
            
    def str(self):
        return self.name + ": " + self.street + ", " + self.city + ", " + self.province